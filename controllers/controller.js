
function renderProductList(productArr) {
    var contentHTML = "";

    productArr.reverse().forEach(function(item){
        var content = `<tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.price}</td>
            <td>${item.img}</td>
            <td>${item.type}</td>
            <td>${item.desc}</td>
            <td>
            <button onclick="deleteSP(${item.id})" class="btn btn-danger">Delete</button>
            <button onclick="editSP(${item.id})" class="btn btn-warning">Edit</button>
            </td>
        </tr>`;
        contentHTML += content;
    });
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;

}

function loadingOn(){
    document.getElementById("spinner").style.display="flex";
}

function loadingOff(){
    document.getElementById("spinner").style.display="none";
}

function getInfoFromForm(){
    var tenSP = document.getElementById("TenSP").value;
    var giaSP = document.getElementById("GiaSP").value;
    var hinhSP = document.getElementById("HinhSP").value;
    var loaiSP = document.getElementById("LoaiSP").value;
    var moTaSP = document.getElementById("moTaSP").value;
    // ko có id trong create
    return {
        name: tenSP,
        price: giaSP,
        img: hinhSP,
        type: loaiSP,
        desc: moTaSP,
    };
}

function showInfoIntoForm(product) {
    document.getElementById("TenSP").value = product.name;
    document.getElementById("GiaSP").value = product.price;
    document.getElementById("HinhSP").value = product.img;
    document.getElementById("LoaiSP").value = product.type;
    document.getElementById("moTaSP").value = product.desc;
}