
// tạo biến tạm để khi update cho biết update sản phẩm có id nào
var idProductUpdate = null;

// lay dssp tu server va render ra layout
function fetchProductList() {
    loadingOn();
    productServ.getList()
 .then(function(res){
    renderProductList(res.data);
    loadingOff();
}) .catch(function(err){
    loadingOff();
})
}
fetchProductList();

// delete
function deleteSP(id) {
    loadingOn();
    productServ.delete(id)
   .then(function(res){
        // sau khi xoa thanh cong tren server thì gọi api lay ds mới nhat tu server
        fetchProductList();
        loadingOff();
    }) .catch(function(err){
        loadingOff();
    })
    console.log("id", id);
}

// add
function addSP() {
    var newProduct = getInfoFromForm();
    productServ.create(newProduct)
  .then(function(res){
    // nếu thêm thành công thì gọi lại api lấy danh sách MỚI NHẤT từ server
    fetchProductList();
    // close modal form after ADD success
    $("#myModal").modal("hide");
  })
  .catch(function(err){
    fetchProductList();
  })
 
}

// edit
function editSP(id) {
    idProductUpdate = id;
    // show modal form when click button update
    $("#myModal").modal("show");
    loadingOn();
    // gọi api lấy thông tin sản phẩm để đưa lên form
    productServ.getByID(id)
    .then(function(res){
        loadingOff();
        // show info into modal form
        showInfoIntoForm(res.data);
    })
    .catch(function(err){
        loadingOff();
    })
   
}

// update
function updateSP() {
    var product = getInfoFromForm();
    productServ.update(idProductUpdate, product)
    .then(function(res){
        fetchProductList();
        $("#myModal").modal("hide");
    })
    .catch(function(err){
        fetchProductList();
    })
}
// promise chaining, promise all