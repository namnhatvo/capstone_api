
// api url
const BASE_URL = "https://64891bbe0e2469c038fea0e9.mockapi.io/capstone_api";

var productServ = {
    getList: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    delete: (id) => {
        return  axios({
        url: `${BASE_URL}/${id}`,
        method: "DELETE",
    });
    },
    create: (product) => {
        return  axios({
            url: BASE_URL,
            method: "POST",
            data: product,
        });
    },
    getByID: function(id) {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
        });
    },
    update: function (id, product) {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PUT",
            data: product,
        });
    },
}